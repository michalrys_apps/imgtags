package com.michalrys.imgtags.model.iim4j;

import com.michalrys.imgtags.model.Metadata;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class model3test {
    private static final String PICUTURE_TEST_PATH = "./src/resources/testing.jpg";

    public static void main(String[] args) {
        File file = new File(PICUTURE_TEST_PATH);

        System.out.println(file.exists());

        try {
            BufferedImage image = ImageIO.read(file);

            System.out.println(image.toString());

            String[] propertyNames = image.getPropertyNames();
            System.out.println(Arrays.toString(propertyNames));

        } catch (IOException e) {
            e.printStackTrace();
        }


        Metadata metadata = new Metadata();
        metadata.readAndDisplayMetadata(PICUTURE_TEST_PATH);

        try {
            ImageReader imageReader = ImageIO.getImageReader((ImageWriter) ImageIO.createImageInputStream(file));



        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
