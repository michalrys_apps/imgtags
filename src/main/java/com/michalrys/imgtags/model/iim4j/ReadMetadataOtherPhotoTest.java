package com.michalrys.imgtags.model.iim4j;

import com.nmote.iim4j.IIM;
import com.nmote.iim4j.IIMFile;
import com.nmote.iim4j.IIMReader;
import com.nmote.iim4j.dataset.*;
import com.nmote.iim4j.stream.FileIIMInputStream;
import com.nmote.iim4j.stream.JPEGIIMInputStream;
import com.nmote.iim4j.stream.JPEGUtil;

import java.io.*;

public class ReadMetadataOtherPhotoTest {
    private static final String PICTURE_TEST_PATH = "./src/resources/testing.jpg";
    private static final String PICTURE_TEST_PATH2 = "./src/resources/testing2.jpg";
    private static final String PICTURE_TEST_PATH3 = "./src/resources/testing3.jpg";
    private static final String PICTURE_TEST_PATH4 = "./src/resources/testing4.jpg";
    private static String format = "JPG";

    public static void main(String[] args) {
        File file = new File(PICTURE_TEST_PATH);
        File file2 = new File(PICTURE_TEST_PATH2);
        File file3 = new File(PICTURE_TEST_PATH3);
        File file4 = new File(PICTURE_TEST_PATH4);
//        Metadata metadata = new Metadata();
//        metadata.readAndDisplayMetadata(PICTURE_TEST_PATH);

        // iim4j

        readTags(file);
        System.out.println("---- insert keywords ----");
//        writeTags();
//        readTags(file2);

        //writeAppendTags(file);
        writeAppendTags(file);
//        writeTags2(file, file2);
        readTags(file2);


        clearTags(file2, file3);

        writeNewTagOnly("ptak, cos jescze, i cos jeszcze", file3, file4);

        

    }

    public static void readTags(File file) {
        try {
            IIMReader reader = new IIMReader(new JPEGIIMInputStream(new FileIIMInputStream(file)));

            IIMFile iimFile = new IIMFile();
            iimFile.readFrom(reader, 20);

            for (DataSet ds : iimFile.getDataSets()) {
                Object value = ds.getValue();
                DataSetInfo info = ds.getInfo();
                System.out.println("-> " + info.toString() + " " + info.getName() + ": " + value);
            }
            System.out.println("----------");
            //System.out.println("TAGS: " + iimFile.getDataSets().get(2).getValue());

        } catch (IOException | InvalidDataSetException e) {
            e.printStackTrace();
        } finally {

        }
    }

    public static void writeTags() {
        IIMFile iimFile = new IIMFile();
        try {
            iimFile.setCharacterSet("UTF8");
            iimFile.add(IIM.KEYWORDS, "cute");
            //iimFile.add(IIM.CODED_CHARACTER_SET, "JPG");
            iimFile.validate();

            InputStream in = new FileInputStream(PICTURE_TEST_PATH);
            OutputStream out = new FileOutputStream(PICTURE_TEST_PATH2);

            JPEGUtil.insertIIMIntoJPEG(out, iimFile, in);
        } catch (IOException | InvalidDataSetException e) {
            e.printStackTrace();
        }

    }

    public static void writeTags2(File file1, File file2) {
        IIMFile iimFile = new IIMFile();

        String encoding = "";
        
        try {
            InputStream inputStream = new FileInputStream(file1);

            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            encoding = inputStreamReader.getEncoding();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("encoding = " + encoding);

        iimFile.setCharacterSet("UTF8");
        //iimFile.setCharacterSet("UTF8");

        DefaultDataSetInfoFactory defaultDataSetInfoFactory = new DefaultDataSetInfoFactory();

        //Autor
        DataSetInfo dataSetInfo = null;
        try {
            dataSetInfo = defaultDataSetInfoFactory.create(IIM.BY_LINE);
        } catch (InvalidDataSetException e) {
            e.printStackTrace();
        }

        String author = "author";
        byte[] data = new byte[0];

        try {
            data = author.getBytes("UTF8");
            //data = author.getBytes("UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        DefaultDataSet defaultDataSet = new DefaultDataSet(dataSetInfo, data);
        iimFile.add(defaultDataSet);


        //Titel
        try {
            dataSetInfo = defaultDataSetInfoFactory.create(IIM.CAPTION_ABSTRACT);
        } catch (InvalidDataSetException e) {
            e.printStackTrace();
        }
        String caption = "caption abstract";
        try {
            data = caption.getBytes("UTF8");
            //data = caption.getBytes("UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        defaultDataSet = new DefaultDataSet(dataSetInfo, data);
        iimFile.add(defaultDataSet);

        //Keywords
        try {
            dataSetInfo = defaultDataSetInfoFactory.create(IIM.KEYWORDS);
        } catch (InvalidDataSetException e) {
            e.printStackTrace();
        }
        String keywords = "keywords";
        try {
            data = keywords.getBytes("UTF8");
            //data = keywords.getBytes("UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        defaultDataSet = new DefaultDataSet(dataSetInfo, data);
        iimFile.add(defaultDataSet);

        System.out.println(iimFile.toString());

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            JPEGUtil.insertIIMIntoJPEG(fos, iimFile, fis);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void writeAppendTags(File file) {
        try {
            IIMReader reader = new IIMReader(new JPEGIIMInputStream(new FileIIMInputStream(file)));

            IIMFile iimFile = new IIMFile();
            iimFile.readFrom(reader, 20);
            iimFile.setCharacterSet("UTF8");

            String keywords = (String) iimFile.get(IIM.KEYWORDS);
            keywords += ", cute";

            System.out.println(keywords);

            iimFile.validate();
            //iimFile.remove(IIM.KEYWORDS);
            iimFile.add(IIM.KEYWORDS, keywords);
            iimFile.validate();

            InputStream in = new FileInputStream(PICTURE_TEST_PATH);
            OutputStream out = new FileOutputStream(PICTURE_TEST_PATH2);

            JPEGUtil.insertIIMIntoJPEG(out, iimFile, in);

        } catch (IOException | InvalidDataSetException e) {
            e.printStackTrace();
        }
    }

    public static void clearTags(File fileRead, File fileWrite) {
        try {
            IIMReader reader = new IIMReader(new JPEGIIMInputStream(new FileIIMInputStream(fileRead)));

            IIMFile iimFile = new IIMFile();
            iimFile.readFrom(reader, 20);
            iimFile.setCharacterSet("UTF8");

            iimFile.remove(IIM.KEYWORDS);
            iimFile.validate();

            InputStream in = new FileInputStream(fileRead);
            OutputStream out = new FileOutputStream(fileWrite);

            JPEGUtil.insertIIMIntoJPEG(out, iimFile, in);

        } catch (IOException | InvalidDataSetException e) {
            e.printStackTrace();
        }
    }

    public static void writeNewTagOnly(String keywords, File fileRead, File fileWrite) {
        try {
            IIMReader reader = new IIMReader(new JPEGIIMInputStream(new FileIIMInputStream(fileRead)));

            IIMFile iimFile = new IIMFile();

            try {
                iimFile.readFrom(reader, 20);
            } catch (Exception e) {
                System.out.println("there is nothing to read");
            }

            iimFile.setCharacterSet("UTF8");
            System.out.println(keywords);

            iimFile.add(IIM.KEYWORDS, keywords);
            iimFile.validate();

            InputStream in = new FileInputStream(fileRead);
            OutputStream out = new FileOutputStream(fileWrite);

            JPEGUtil.insertIIMIntoJPEG(out, iimFile, in);

        } catch (IOException | InvalidDataSetException e) {
            e.printStackTrace();
        }
    }
}
