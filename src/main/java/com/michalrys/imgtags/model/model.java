package com.michalrys.imgtags.model;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.iptc.IptcDirectory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class model {

    private static final String PICUTURE_TEST_PATH = "./src/resources/testing.jpg";

    public static void main(String[] args) {
        //Metadata metadata = new Metadata();
        //metadata.readAndDisplayMetadata(PICUTURE_TEST_PATH);

        System.out.println("This is test for reading picture meta data.");

        File file = new File(PICUTURE_TEST_PATH);

        Metadata metadata = null;

        try {
            metadata = ImageMetadataReader.readMetadata(file);
        } catch (ImageProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        metadata.getDirectories().forEach(s -> System.out.println(s));

        List<Directory> directories = new ArrayList<>();
        directories.addAll((Collection<? extends Directory>) metadata.getDirectories());

        List<String> keywords = ((IptcDirectory) directories.get(7)).getKeywords();
        System.out.println(keywords);
        System.out.println(keywords.get(0));

        System.out.println("--------------");



    }
}
